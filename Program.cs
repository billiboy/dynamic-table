﻿using System;

namespace Table
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество строк таблицы");
            int n = GetValidNumber();
            Console.WriteLine("Введите количество столбцов таблицы");
            int m;
            do
            {
                m = GetValidNumber();
                if (m > 59) Console.WriteLine("Консоль может вывести не более 59 стобцов в связи с ограничением размера экрана, введите число меньшее 59");
            } while (m > 59);

            int[,] numbers = new int[n,m];
            Random randomNumber = new Random();
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    numbers[i, j] = randomNumber.Next(1, 1000);
       
            int[] max = new int[m];

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    if (Convert.ToString(numbers[i, j]).Length > max[j]) max[j] = Convert.ToString(numbers[i, j]).Length;

            int height = GetHeight(n);
            int width = GetWidth(m, max);
            Console.WriteLine("Hello World!");
            int number = 0;

            for (int i = 1; i <= height; i++)
            {
                if (i == 1) GetUpperBound(width, max);
                else if (i == height) GetLowerBound(width, max);
                else if ((i % 2 != 0)&(i != height)) GetStraightLine(width, max);            
                else
                {
                    GetWriteData(width, height, max, numbers,number);
                    number++;
                }
            }
        }

        static int GetValidNumber()
        {
            int n = 0;
            do
            {

                string tryP = Console.ReadLine();
                int.TryParse(tryP, out n);
                if (n == 0) Console.WriteLine("Вы ввели неверные данные, попробуйте снова");
                if (n == 0) Console.WriteLine("Вы ввели неверные данные, попробуйте снова");
            } while (n == 0);
            return n;
        }

        static int GetWidth(int m, int[] max)
        {
            int width = m;
            for (int i = 0; i < m; i++)
            {
                width += max[i];
            }
            Console.WriteLine(width);
            return width + 1;
        }

        static int GetHeight(int n)
        {
            Console.WriteLine(n * 2);
            return n * 2 + 1;    
        }

        static void GetUpperBound(int width, int[] max)
        {
            int k = 0;
            int n = 0;
            Console.Write("╔");
            for (int i = 0; i <= width - 3; i++)
            {
                if (n == max[k])
                {
                    Console.Write("╦");
                    if (k + 1 < max.Length) k++;
                    n = -1;
                }
                else
                {
                    Console.Write("═");
                }
                n++;
            }
            Console.WriteLine("╗");
        }

        static void GetStraightLine(int width, int[] max)
        {
            int k = 0;
            int n = 0;
            Console.Write("╠");
            for (int i = 0; i <= width - 3; i++)
            {
                if (n == max[k])
                {
                    Console.Write("╬");
                    if (k + 1 < max.Length) k++;
                    n = -1;
                }
                else
                {
                    Console.Write("═");
                }
                n++;
            }
            Console.WriteLine("╣");
        }

        static void GetLowerBound(int width, int[] max)
        {
            int k = 0;
            int n = 0;
            Console.Write("╚");
            for (int i = 0; i <= width - 3; i++)
            {
                if (n == max[k])
                {
                    Console.Write("╩");
                    if (k + 1 < max.Length) k++;
                    n = -1;
                }
                else
                {
                    Console.Write("═");
                }
                n++;
            }
            Console.WriteLine("╝");
        }

        static void GetWriteData(int width, int height, int[] max, int[,] numbers, int number)
        {
            Console.Write("║");
            for (int j = 0; j < max.Length; j++)
            {
                Console.Write(numbers[number,j]);
                for (int i = 0; i < max[j] - Convert.ToString(numbers[number, j]).Length; i++) Console.Write(" ");
                Console.Write("║");
            }
            Console.WriteLine();
        }
    }
}
